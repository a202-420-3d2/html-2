const express = require('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/public/index.html`);
});

app.get('/contact', (req, res) => {
  res.sendFile(`${__dirname}/public/contact.html`);
});

app.get('/services', (req, res) => {
  res.sendFile(`${__dirname}/public/services.html`);
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
